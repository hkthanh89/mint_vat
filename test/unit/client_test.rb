require 'test_helper'
require_relative '../../spec/spec_helper'

class ClientTest < ActiveSupport::TestCase
  test 'should be valid client' do
    nothing_else = {}
    valid_client_with nothing_else do |client|
      client.should be_valid
    end
  end

  test 'should not be valid without name' do
    valid_client_with :name => nil do |client|
      client.should_not be_valid
    end
  end

  test 'should not be valid without user' do
    valid_client_with nil do |client|
      client.user = nil
      client.should_not be_valid
    end
  end

  test 'should load client by user' do
    client = Client.find_by_user_id 1
    client.should_not be_nil
  end

  test 'should not load client by unknown user' do
    client = Client.find_by_user_id -1
    client.should be_nil
  end

  private
  def valid_client_with(hash)
    client = Client.new(attributes_for(:client).merge((hash ? hash : {})))
    client.user = build(:user)
    yield client
  end
end