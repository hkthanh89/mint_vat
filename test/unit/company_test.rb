require 'test_helper'
require_relative '../../spec/spec_helper'

class CompanyTest < ActiveSupport::TestCase
  test 'should be valid company' do
    nothing_else = {}
    valid_company_with nothing_else do |company|
      company.should be_valid
    end
  end

  test 'should not be valid without name' do
    valid_company_with :name => nil do |company|
      company.should_not be_valid
    end
  end
  
  test 'should not be valid without user' do
    valid_company_with nil do |company|
      company.user = nil
      company.should_not be_valid
    end
  end

  test 'should load company by user' do
    company = Company.find_by_user_id 1
    company.should_not be_nil
  end

  test 'should not load company by unknown user' do
    company = Company.find_by_user_id -1
    company.should be_nil
  end

  private
  def valid_company_with(hash)
    company = Company.new(attributes_for(:company).merge((hash ? hash : {})))
    company.user = build(:user)
    yield company
  end
end