require 'test_helper'
require_relative '../../spec/spec_helper'

class InvoiceTest < ActiveSupport::TestCase
  test 'should be valid invoice' do
    nothing_else = {}
    valid_invoice_with nothing_else do |invoice|
      invoice.should be_valid
    end
  end

  test 'should not be valid without number' do
    valid_invoice_with :number => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without date' do
    valid_invoice_with :date => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without vat rate' do
    valid_invoice_with nil do |invoice|
      invoice.vat_rate = nil # have to set it after initialization otherwise is automatically set to 0
      invoice.should_not be_valid
    end
  end

  test 'should not be valid with negative vat rate' do
    valid_invoice_with :vat_rate => -1 do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid with vat rate 100 or more' do
    valid_invoice_with :vat_rate => 100 do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without self name' do
    valid_invoice_with :self_name => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without client name' do
    valid_invoice_with :client_name => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should have vat_rate 0 by default' do
    assert_equal 0, Invoice.new.vat_rate
  end

  test 'should not be valid without total' do
    valid_invoice_with :inv_total => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid with negative total' do
    valid_invoice_with :inv_total => -1 do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without vat total' do
    valid_invoice_with :inv_vat_total => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid with negative vat total' do
    valid_invoice_with :inv_vat_total => -1 do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without grand total' do
    valid_invoice_with :inv_grand_total => nil do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid with negative grand total' do
    valid_invoice_with :inv_grand_total => -1 do |invoice|
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without rows' do
    valid_invoice_with nil do |invoice|
      invoice.invoice_rows = []
      invoice.should_not be_valid
    end
  end

  test 'should not be valid without user' do
    valid_invoice_with nil do |invoice|
      invoice.user = nil
      invoice.should_not be_valid
    end
  end

  test 'should load invoices by user' do
    invoices = Invoice.find_all_by_user_id 1
    invoices.should_not be_empty
  end

  test 'should calculate totals' do
    valid_invoice_with nil do |invoice|
      invoice.vat_rate = 20
      row1 = InvoiceRow.new({:name => 'p1', :quantity => 3, :unit_price => 3.5})
      row1.calculate_net_price
      row2 = InvoiceRow.new({:name => 'p2', :quantity => 0.5, :unit_price => 40})
      row2.calculate_net_price
      invoice.invoice_rows = [row1, row2]

      invoice.calculate_totals

      invoice.inv_total.should eq(30.5)
      invoice.inv_vat_total.should eq(6.1)
      invoice.inv_grand_total.should eq(36.6)
    end
  end

  test 'should add and save new row' do
    invoice = Invoice.find(1)
    invoice.should be_valid
    invoice_row = invoice.invoice_rows.build({
      :name =>'Product 2',
      :quantity =>'1.86',
      :unit_price =>'1.99',
      :unit =>'lbs'
    })
    invoice_row.calculate_net_price
    invoice.save!

    updated_invoice = Invoice.find_by_number invoice.number
    updated_invoice.invoice_rows.count.should eq(2)

    updated_invoice.invoice_rows[1].name.should eq(invoice_row.name)
  end

  test 'should compute the next number' do
    Invoice.next_number(1).should eq('12')
  end

  private
  def valid_invoice_with(hash)
    invoice = Invoice.new(attributes_for(:invoice).merge((hash ? hash : {})))
    invoice.invoice_rows = [build(:invoice_row)]
    invoice.user = build(:user)
    yield invoice
  end
end