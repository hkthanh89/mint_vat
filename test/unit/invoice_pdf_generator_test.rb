require 'test_helper'
require_relative '../../spec/spec_helper'

class InvoicePdfGeneratorTest < ActiveSupport::TestCase
  test 'should generate pdf file from invoice' do
    invoice = Invoice.find(1)
    InvoicePdfGenerator.new(invoice).write_pdf_file
    # TODO figure out what to assert here
  end
end