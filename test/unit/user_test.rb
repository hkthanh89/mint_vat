require 'test_helper'
require_relative '../../spec/spec_helper'

class UserTest < ActiveSupport::TestCase

  test 'should not be valid without email' do
    valid_user_with :email => nil do |user|
      user.should_not be_valid
    end
    valid_user_with :email => '' do |user|
      user.should_not be_valid
    end
  end

  test 'should not be valid without password' do
    valid_user_with :password => nil do |user|
      user.should_not be_valid
    end
    valid_user_with :password => '' do |user|
      user.should_not be_valid
    end
  end

  test 'should not be valid without client ip' do
    valid_user_with :client_ip => nil do |user|
      user.should_not be_valid
    end
    valid_user_with :client_ip => '' do |user|
      user.should_not be_valid
    end
  end

  test 'should be valid user' do
    nothing_else = {}
    valid_user_with nothing_else do |user|
      user.should be_valid
    end
  end

  test 'should authenticate existing user with correct password' do
    user = User.authenticate 'existing@email.com', '123'
    assert user
  end

  test 'should fail authentication of existing user with wrong password' do
    user = User.authenticate 'existing@email.com', 'wrong_password'
    assert !user
  end

  test 'should fail authentication of non-existing' do
    user = User.authenticate 'non_existing@email.com', '123'
    assert !user
  end

  private
  def valid_user_with(hash)
    yield User.new(attributes_for(:user).merge((hash ? hash : {})))
  end

end
