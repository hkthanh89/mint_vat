require 'test_helper'
require_relative '../../spec/spec_helper'

class InvoiceRowTest < ActiveSupport::TestCase

  test 'should not be valid without name' do
    valid_invoice_row_with :name => nil do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid without quantity' do
    valid_invoice_row_with :quantity => nil do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid with negative quantity' do
    valid_invoice_row_with :quantity => -1 do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid without unit price' do
    valid_invoice_row_with :unit_price => nil do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid with negative unit price' do
    valid_invoice_row_with :unit_price => -1 do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid without net price' do
    valid_invoice_row_with :net_price => nil do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should not be valid with negative net price' do
    valid_invoice_row_with :net_price => -1 do |invoice_row|
      invoice_row.should_not be_valid
    end
  end

  test 'should calculate net price' do
    valid_invoice_row_with :quantity => 3.2, :unit_price => 4.5 do |invoice_row|
      invoice_row.calculate_net_price
      invoice_row.net_price.should eq(14.4)
    end
  end

  test 'should not break when calculating net price out of invalid values' do
    valid_invoice_row_with :quantity => 'xyz', :unit_price => nil do |invoice_row|
      invoice_row.calculate_net_price
      invoice_row.net_price.should eq(0)
    end
  end

  private
  def valid_invoice_row_with(hash)
    yield InvoiceRow.new(attributes_for(:invoice_row).merge((hash ? hash : {})))
  end

end