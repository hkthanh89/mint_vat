require 'test_helper'
require_relative '../../spec/spec_helper'

class ExpenseTest < ActiveSupport::TestCase
	test 'should be valid expense' do
		nothing_else = {}
		valid_expense_with nothing_else do |expense|
			expense.should be_valid
		end
	end

	test 'should not be valid without reference' do
		valid_expense_with :reference => nil do |expense|
			expense.should_not be_valid
		end
	end

	test 'should not be valid without date' do
		valid_expense_with :date => nil do |expense|
			expense.should_not be_valid
		end
	end

	test 'should not be valid without description' do
		valid_expense_with :description => nil do |expense|
			expense.should_not be_valid
		end
	end

	test 'should not be valid without amount' do
		valid_expense_with :amount => nil do |expense|
			expense.should_not be_valid
		end
	end

	test 'should not be valid without vat' do
		valid_expense_with :vat => nil do |expense|
			expense.should_not be_valid
		end
	end

  test 'should not be valid without user' do
    valid_expense_with nil do |expense|
      expense.user = nil
      expense.should_not be_valid
    end
  end

  test 'should have vat 0 by default' do
  	assert_equal 0, Expense.new.vat
  end

  test 'should load expenses by user' do
  	expenses = Expense.find_all_by_user_id 1
  	expenses.should_not be_empty
  end

	private
	def valid_expense_with(hash)
		expense = Expense.new(attributes_for(:expense).merge((hash ? hash : {})))
		expense.user = build(:user)
		yield expense
	end
end