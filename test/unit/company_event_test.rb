require 'test_helper'
require_relative '../../spec/spec_helper'

class CompanyEventTest < ActiveSupport::TestCase
	test 'should be valid company event' do
		nothing_else = {}
		valid_company_event_with nothing_else do |company_event|
			company_event.should be_valid
		end
	end

	test 'should not be valid without company' do
		valid_company_event_with nil do |company_event|
			company_event.company = nil
			company_event.should_not be_valid
		end
	end

	test 'should not be valid without date' do
		valid_company_event_with :date => nil do |company_event|
			company_event.should_not be_valid
		end
	end

	test 'should not be valid without event' do
		valid_company_event_with nil do |company_event|
			company_event.event = nil
			company_event.should_not be_valid
		end
	end

	test 'should not be valid without date beginning of month' do
		valid_company_event_with :date => '2013-06-03' do |company_event|
			company_event.should_not be_valid
		end
	end

	test 'should have new_flat_rate 0 by default' do
		assert_equal 0, CompanyEvent.new.new_flat_rate
	end

	test 'should have flat_rate_1 0 by default' do
		assert_equal 0, CompanyEvent.new.flat_rate_1
	end

	test 'should have flat_rate_2 0 by default' do
		assert_equal 0, CompanyEvent.new.flat_rate_2
	end

	test "should load company events of user's company" do 
		company_events = CompanyEvent.find_all_by_company_id 1
		company_events.should_not be_empty
	end

	private
	def valid_company_event_with(hash)
		company_event = CompanyEvent.new(attributes_for(:company_event).merge((hash ? hash : {})))
		company_event.event = events(:one)
		company_event.company = companies(:one)
		yield company_event		
	end
end