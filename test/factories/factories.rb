FactoryGirl.define do
  factory :invoice_row do
    name 'Product'
    quantity 1.0
    unit_price 1.0
    net_price 1.0
  end

  factory :invoice do
    number 'AA 123'
    date Date.today
    client_name 'MyClient Limited'
    self_name 'My Limited'
    inv_total 1.2
    inv_vat_total 0.2
    inv_grand_total 1.0
  end

  factory :user do
    email 'my@email.com'
    password 'passw0rd'
    client_ip '127.0.0.1'
  end

  factory :company do
    name 'My Company'
    address_1 'Company address'
    town 'Company town'
    postcode 'XXX YYY'
  end

  factory :client do
    name 'Client Company'
  end

  factory :expense do
    reference "MyString"
    date Date.today
    description "MyString"
    amount "9.99"
    vat "9.99"
    provider "MyString"
    provider_vat_regno "MyString"
    justification "MyText"
  end

  factory :event do
    name "Registered VAT"
  end

  factory :company_event do
    date "2013-05-01"
    new_flat_rate "9.99"
    flat_rate_1 "9.99"
    flat_rate_2 "9.99"
  end
end