require 'test_helper'
require_relative '../../spec/spec_helper'

class CompanyEventsControllerTest < ActionController::TestCase
	test 'should get index' do
		login_user 1
		get :index
		assert_response :success
	end

	test 'should not be able to access page unless logged in' do
		user_not_logged_in
		get :index
		assert_response 302
	end

	test 'should load company event for editing' do
		login_user 1
		get :edit, { :id => 1 }
		assigns(:company_event).should_not be_nil
	end

	test 'should suggest company event date' do
		login_user 1
		get :index
		assert_select "a#add_event"
		assigns(:company_event).date.should_not be_nil
	end

	test 'should create company event with valid information' do
		login_user 1
		params = { :company_event => {:date => '07-2013',
																	:event_id => '1'
																 } 
							}
		post :create, params
		assigns(:company_event).should be_valid

		create_company_event = CompanyEvent.find_by_date('2013-07-01')
		create_company_event.should_not be_nil
	end

	test 'should create company event applied for flat rate scheme with two optional fields flat rate' do
		login_user 1
		params = { :company_event => {:date => '07-2013',
																	:event_id => '2',
																	:flat_rate_1 => 10.5,
																	:flat_rate_2 => 13.5
																 } 
							}
		post :create, params
		assigns(:company_event).should be_valid

		create_company_event = CompanyEvent.find_by_date('2013-07-01')
		create_company_event.should_not be_nil
		create_company_event.flat_rate_1.should eq(10.5)
		create_company_event.flat_rate_2.should eq(13.5)
	end

	test 'should not create company event applied for flat rate scheme without two optional fields flat rate' do
		login_user 1
		get :index
		params = { :company_event => {:date => '09-2013',
																	:event_id => '2',
																	:flat_rate_1 => '',
																	:flat_rate_2 => ''
																 } 
							}
		post :create, params
		assigns(:company_event).should_not be_valid
	end

	test 'should create company event the flat rate changed with optional field new_flat_rate' do
		login_user 1
		params = { :company_event => {:date => '07-2013',
																	:event_id => '4',
																	:new_flat_rate => '15'
																 } 
							}
		post :create, params
		assigns(:company_event).should be_valid

		create_company_event = CompanyEvent.find_by_date('2013-07-01')
		create_company_event.should_not be_nil
		create_company_event.new_flat_rate.should eq(15)
	end

	test 'should not create company event the flat rate changed without optional field new_flat_rate' do
		login_user 1
		get :index
		params = { :company_event => {:date => '07-2013',
																	:event_id => '4',
																	:new_flat_rate => ''
																 } 
							}
		post :create, params
		assigns(:company_event).should_not be_valid
	end

	test 'should not create company_event with invalid data' do
		login_user 1
		get :index
		params = { :company_event => {:date => '07-2013',
																	:event_id => 2,
																	:flat_rate_1 => '-2',
																	:flat_rate_2 => 'test'
																 }

							}
		post :create, params
		assigns(:company_event).should_not be_valid
		assigns(:company_event).errors.size.should eq(2)
	end

	test 'should update company_event' do
		login_user 1
		params = 
			{
				:id => 1,
				:company_event => {
					:date => '08-2013',
					:event_id => 5
				}
			}
		put :update, params

		assigns(:company_event).should be_valid

		update_company_event = CompanyEvent.find('1')
		update_company_event.should_not be_nil

		update_company_event.date.strftime("%Y-%m-%d").should eq('2013-08-01')
		update_company_event.event_id.should eq(5)
	end

	test 'should not update company_event without event' do
		login_user 1
		get :index
		params = 
			{
				:id => 1,
				:company_event => {
					:date => '08-2013',
					:event_id => ''
				}
			}
		put :update, params

		assigns(:company_event).should_not be_valid

		update_company_event = CompanyEvent.find('1')
		update_company_event.should_not be_nil

		update_company_event.date.strftime("%Y-%m-%d").should eq('2013-06-03')
		update_company_event.event_id.should eq(1)
	end

	test 'should not update company_event with invalid data' do
		login_user 1
		get :index
		params = 
			{
				:id => 1,
				:company_event => {
					:date => '08-2013',
					:event_id => '2',
					:flat_rate_1 => '-2',
					:flat_rate_2 => 'test'
				}
			}
		put :update, params

		assigns(:company_event).should_not be_valid
		assigns(:company_event).errors.size.should eq(2)

		update_company_event = CompanyEvent.find('1')
		update_company_event.should_not be_nil

		update_company_event.date.strftime("%Y-%m-%d").should eq('2013-06-03')
		update_company_event.event_id.should eq(1)
	end
end