require 'test_helper'
require_relative '../../spec/spec_helper'

class HomeControllerTest < ActionController::TestCase

  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'should sign up new user' do
    email = 'test@email.com'
    post :signup, :sign_up => {
        :username => '',
        :email => email,
        :password => '123',
        :password_confirmation => '123'
    }
    assert_not_nil session[:user_id]
    assert User.find_by_email email
  end

  test 'should not sign up new user with honeytrap username' do
    post :signup, :sign_up => {
        :username => 'i_am_a_mindless_bot',
        :email => 'test@email.com',
        :password => '123',
        :password_confirmation => '123'
    }
    flash[:errors].messages[:username].should_not be_empty
    assert_nil session[:user_id]
  end

  test 'should not sign up new user with no email' do
    post :signup, :sign_up => {
        :username => '',
        :email => '',
        :password => '123',
        :password_confirmation => '123'
    }
    flash[:errors].messages[:email].should_not be_empty
    assert_nil session[:user_id]
  end

  test 'should not sign up new user with unconfirmed password' do
    post :signup, :sign_up => {
        :username => '',
        :email => 'test@email.com',
        :password => '123',
        :password_confirmation => 'different'
    }
    flash[:errors].messages[:password].should_not be_empty
    assert_nil session[:user_id]
  end

  test 'should not sign up new user with existing email' do
    post :signup, :sign_up => {
        :username => '',
        :email => 'existing@email.com',
        :password => '123',
        :password_confirmation => '123'
    }
    flash[:errors].messages[:email].should_not be_empty
    assert_nil session[:user_id]
  end

  test 'should login existing user with the correct password' do
    post :login, :login => {
        :email => 'existing@email.com',
        :password => '123'
    }
    assert_not_nil session[:user_id]
  end

  test 'should not login existing user with incorrect password' do
    post :login, :login => {
        :email => 'existing@email.com',
        :password => 'incorrect_password'
    }
    assert_nil session[:user_id]
  end

  test 'should not login non-existing user' do
    post :login, :login => {
        :email => 'non_existing@email.com',
        :password => '123'
    }
    assert_nil session[:user_id]
  end

  test 'should redirect logged in user to welcome page' do
    login_user 1
    get :index
    assert_redirected_to :controller => :welcome, :action => :index
  end

  test 'should logout user' do
    login_user 1
    get :logout
    assert_redirected_to :controller => :home, :action => :index
    assert_nil session[:user_id]
  end

end
