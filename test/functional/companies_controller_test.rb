require 'test_helper'
require_relative '../../spec/spec_helper'

class CompaniesControllerTest < ActionController::TestCase
  test 'should get index' do
    login_user 1
    get :index
    assert_response :success
  end

  test 'should not be able to access page unless logged in' do
    user_not_logged_in
    get :index
    assert_response 302
  end

  test 'should get new company when user doesn''t have one' do
    login_user 2
    get :index
    assigns(:company).should be_new_record
  end

  test 'should get user company on company index page' do
    login_user 1
    get :index
    assigns(:company).should_not be_new_record
  end

  test 'should create company if user doesn''t have one' do
    login_user 2
    params = {:company =>{:name => 'New company', :vat_code => 'New vat', :address_1 => 'New address1',  :address_2 => 'New address2',
                          :town => 'New town', :postcode => 'NNN NNN', :county => 'New county',
                          :bank => 'New bank', :account => 'New account', :sort_code => 'New sort code'}}
    post :modify, params

    assigns(:company).should be_valid

    new_company = Company.find_by_name('New company')
    new_company.should_not be_nil
    assert_company_matches(new_company, params[:company])
  end

  test 'should modify company' do
    login_user 1
    params = {:company =>{:name => 'Updated company', :vat_code => 'Updated vat', :address_1 => 'Updated address1',  :address_2 => 'Updated address2',
                          :town => 'Updated town', :postcode => 'UUU UUU', :county => 'Updated county',
                          :bank => 'Updated bank', :account => 'Updated account', :sort_code => 'Updated sort code'}}
    put :modify, params

    assigns(:company).should be_valid

    saved_company = Company.find_by_name('Updated company')
    saved_company.should_not be_nil
    assert_company_matches(saved_company, params[:company])
  end

  private
  def assert_company_matches(newly_created, params)
    newly_created.name.should eq(params[:name])
    newly_created.vat_code.should eq(params[:vat_code])
    newly_created.address_1.should eq(params[:address_1])
    newly_created.address_2.should eq(params[:address_2])
    newly_created.town.should eq(params[:town])
    newly_created.postcode.should eq(params[:postcode])
    newly_created.county.should eq(params[:county])
    newly_created.bank.should eq(params[:bank])
    newly_created.account.should eq(params[:account])
    newly_created.sort_code.should eq(params[:sort_code])
  end
end
