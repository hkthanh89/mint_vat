require 'test_helper'
require_relative '../../spec/spec_helper'

class InvoicesControllerTest < ActionController::TestCase
  test 'should get index' do
    login_user 1
    get :index
    assert_response :success
  end

  test 'should not be able to access page unless logged in' do
    user_not_logged_in
    get :index
    assert_response 302
  end

  test 'should load invoice for editing' do
    login_user 1
    get :edit, {:id => 1}
    assigns(:invoice).should_not be_nil
  end

  test 'should suggest invoice number' do
    login_user 1
    get :new
    assigns(:invoice).number.should_not be_nil
  end

  test 'should have print_payment_details true by default' do
    login_user 1
    get :new
    assigns(:invoice).print_payment_details.should be_true
  end

  test 'should suggest invoice date' do
    login_user 1
    get :new
    assigns(:invoice).date.should_not be_nil
  end

  test 'should load clients as JSON when create invoice' do
    login_user 1
    get :new
    assert_clients_are_sent_as_json_for_user 1
  end

  test 'should create invoice with rows' do
    login_user 1
    params = {:invoice =>{:number =>'100', :date =>'15/02/2013', :vat_rate =>'0', :self_name =>'MyCompany', :client_name =>'MyClient'},
              :new_invoice_rows =>[{:name =>'A', :quantity =>'1', :unit_price =>'1', :unit =>'1'}],
              :client_id => '1'}
    post :create, params

    assigns(:invoice).should be_valid

    created_invoice = Invoice.find_by_number('100')
    created_invoice.should_not be_nil

    created_invoice.invoice_rows.count.should eq(1)
  end

  test 'should update invoice with rows' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany_updated',
                :client_name => 'BT'
            },
            :invoice_rows => {
                '1' => {
                    :name => 'Product 1',
                    :quantity => '10',
                    :unit_price => '2',
                    :unit => ''
                }
            },
            :client_id => '1'
        }
    put :update, params

    assigns(:invoice).should be_valid

    updated_invoice = Invoice.find(1)
    updated_invoice.should_not be_nil
    updated_invoice.self_name.should eq('MyCompany_updated')
    updated_invoice.inv_vat_total.should eq(4)
    updated_invoice.inv_grand_total.should eq(24)

    updated_invoice.invoice_rows.count.should eq(1)

    assert_invoice_row_matches updated_invoice.invoice_rows[0], params[:invoice_rows]['1']
  end

  test 'should update invoice adding rows' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :client_name => 'BT'
            },
            :invoice_rows => {
                '1' => {
                    :name => 'Product 1',
                    :quantity => '10',
                    :unit_price => '1',
                    :unit => ''
                }
            },
            :new_invoice_rows => [{
                                      :name =>'Product 2',
                                      :quantity =>'1.86',
                                      :unit_price =>'1.99',
                                      :unit =>'lbs'
                                  }],
            :client_id => '1'
        }
    put :update, params

    assigns(:invoice).should be_valid

    updated_invoice = Invoice.find(1)
    updated_invoice.should_not be_nil

    updated_invoice.invoice_rows.count.should eq(2)

    assert_invoice_row_matches updated_invoice.invoice_rows[1], params[:new_invoice_rows][0]
  end

  test 'should update invoice removing rows and adding new' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :client_name => 'BT'
            },
            :new_invoice_rows => [{
                                      :name =>'Product 2',
                                      :quantity =>'1.86',
                                      :unit_price =>'1.99',
                                      :unit =>'lbs'
                                  }],
            :client_id => '1'
        }
    put :update, params

    assigns(:invoice).should be_valid

    updated_invoice = Invoice.find(1)
    updated_invoice.should_not be_nil

    updated_invoice.invoice_rows.count.should eq(1)

    assert_invoice_row_matches updated_invoice.invoice_rows[0], params[:new_invoice_rows][0]
  end

  test 'should create client when save invoice and client doesn''t exist' do
    login_user 2
    params =
        {
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :client_name => 'BT'
            },
            :client => {
                :name =>'New client name',
                :vat_code => '987 654 321',
                :address_1 => 'New client address1',
                :address_2 => 'New client address2',
                :town => 'New client town',
                :postcode => 'YYY XXX',
                :county => 'New client county',
            }
        }
    post :create, params

    @client = Client.find_by_name 'New client name'
    @client.should_not be_nil

    assert_client_matches @client, params[:client]
  end

  test 'should update client when save invoice and client exists' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :client_name => 'BT',

            },
            :client_id => 1,
            :client => {
                :name =>'Updated client name',
                :vat_code => '987 654 321',
                :address_1 => 'Updated client address1',
                :address_2 => 'Updated client address2',
                :town => 'Updated client town',
                :postcode => 'YYY XXX',
                :county => 'Updated client county'
            }
        }
    post :create, params

    client = Client.find_by_name 'Updated client name'
    client.should_not be_nil

    assert_client_matches client, params[:client]
  end

  test 'should update invoice client data when save invoice' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :client_name => 'BT',
                :client_vat_code => '123 456 789',
                :client_address_1 => 'Client address 1',
                :client_address_2 => 'Client address 2',
                :client_town => 'Client town',
                :client_postcode => 'AAA BBB',
                :client_county => 'Client county'
            },
            :client_id => 1,
            :client => {
                :name =>'Updated client name',
                :vat_code => '987 654 321',
                :address_1 => 'Updated client address1',
                :address_2 => 'Updated client address2',
                :town => 'Updated client town',
                :postcode => 'YYY XXX',
                :county => 'Updated client county'
            }
        }
    post :update, params

    client = Client.find 1
    invoice = Invoice.find 1

    assert_invoice_client_data_matches_client invoice, client
  end

  test 'should load clients as JSON when edit invoice' do
    login_user 1
    get :edit, {:id => 1}
    assert_clients_are_sent_as_json_for_user 1
  end

  test 'should create company when save invoice and company doesn''t exist' do
    login_user 2
    params =
        {
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'MyCompany',
                :self_address_1 => '31 Main Rd',
                :self_address_2 => 'Holborn',
                :self_town => 'London',
                :self_postcode => 'EC1A 2RT',
                :client_name => 'BT'
            },
            :client => {
                :name =>'New client name'
            }
        }
    post :create, params

    company = Company.find_by_user_id(2)
    company.should_not be_nil

    assert_company_matches company, params[:invoice]
  end

  test 'should update company when save invoice and company exists' do
    login_user 1
    params =
        {
            :id => 1,
            :invoice => {
                :number => '1',
                :date => '01/01/2013',
                :vat_rate => '20',
                :self_name => 'My Other Company',
                :self_address_1 => '31 Main Rd',
                :self_address_2 => 'Holborn',
                :self_town => 'London',
                :self_postcode => 'EC1A 2RT',
                :client_name => 'BT',

            },
            :client_id => 1,
            :client => {
                :name =>'Updated client name'
            }
        }
    post :create, params

    company = Company.find_by_user_id(1)
    company.should_not be_nil

    assert_company_matches company, params[:invoice]
  end

  private
  def assert_invoice_row_matches(newly_created_row, params)
    newly_created_row.name.should eq(params[:name])
    newly_created_row.quantity.should eq(params[:quantity].to_f)
    newly_created_row.unit_price.should eq(params[:unit_price].to_f)
    newly_created_row.net_price.should_not be_nil
  end

  def assert_client_matches(modified_client, client)
    modified_client.name.should eq(client[:name])
    modified_client.vat_code.should eq(client[:vat_code])
    modified_client.address_1.should eq(client[:address_1])
    modified_client.address_2.should eq(client[:address_2])
    modified_client.town.should eq(client[:town])
    modified_client.postcode.should eq(client[:postcode])
    modified_client.county.should eq(client[:county])
  end

  def assert_invoice_client_data_matches_client(invoice, client)
    invoice.client_name.should eq(client.name)
    invoice.client_vat_code.should eq(client.vat_code)
    invoice.client_address_1.should eq(client.address_1)
    invoice.client_address_2.should eq(client.address_2)
    invoice.client_town.should eq(client.town)
    invoice.client_postcode.should eq(client.postcode)
    invoice.client_county.should eq(client.county)
  end

  def assert_clients_are_sent_as_json_for_user(user_id)
    clients = Client.find_all_by_user_id user_id

    assigns(:clients_json).should eq(InvoicesController.clients_json clients)
  end

  def assert_company_matches(modified_company, params)
    puts params
    modified_company.name.should eq(params[:self_name])
    modified_company.address_1.should eq(params[:self_address_1])
    modified_company.address_2.should eq(params[:self_address_2])
    modified_company.town.should eq(params[:self_town])
    modified_company.postcode.should eq(params[:self_postcode])
  end
end
