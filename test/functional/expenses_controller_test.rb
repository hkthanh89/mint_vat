require 'test_helper'
require_relative '../../spec/spec_helper'

class ExpensesControllerTest < ActionController::TestCase
	test 'should get index' do
		login_user 1
		get :index
		assert_response :success
	end

	test 'should not be able to access page unless logged in' do
		user_not_logged_in
		get :index
		assert_response 302
	end

	test 'should load expense for editing' do
		login_user 1
		get :edit, {:id => 1}
		assigns(:expense).should_not be_nil
	end

	test 'should suggest expense number' do
		login_user 1
		get :new
		assigns(:expense).reference.should_not be_nil
	end

	test 'should suggest expense date' do
		login_user 1
		get :new
		assigns(:expense).date.should_not be_nil
	end

	test 'should create expense without detail information of provider' do
		login_user 1
		params = {:expense => {:reference => "1", 
													 :date => "12/05/2013", 
													 :description => "Internet monthly", 
													 :amount => "400000", 
													 :vat => "100"
													}
							}
		post :create, params

		assigns(:expense).should be_valid

		created_expense = Expense.find_by_reference('1')
		created_expense.should_not be_nil
	end

	test 'should create expense with detail information of provider' do
		login_user 1
		params = {:expense => {:reference => "2", 
													 :date => "12/05/2013", 
													 :description => "Internet monthly", 
													 :amount => "400000", 
													 :vat => "100", 
													 :provider => "FPT", 
													 :provider_vat_regno => "FPT001", 
													 :justification=> "Nothing"
													}
							}
		post :create, params

		assigns(:expense).should be_valid

		created_expense = Expense.find_by_reference('2')
		created_expense.should_not be_nil
	end

	test 'should update expense' do
		login_user 1
		params = 
			{
				:id => 1,
				:expense => {
					:reference => "1", 
				 	:date => "12/05/2013", 
				 	:description => "Internet monthly", 
				 	:amount => "900000", 
				 	:vat => "50000",
				 	:provider => 'VNPT',
				 	:provider_vat_regno => 'VNPT002',
				 	:justification => 'Nothing'
				}
			}
		put :update, params

		assigns(:expense).should be_valid

		updated_expense = Expense.find('1')
		updated_expense.should_not be_nil

		updated_expense.amount.should eq(900000)
		updated_expense.vat.should eq(50000)
		updated_expense.provider.should eq('VNPT')
		updated_expense.provider_vat_regno.should eq('VNPT002')
	end

	test 'should update expense without provider information' do
		login_user 1
		params = 
			{
				:id => 1,
				:expense => {
					:reference => "1", 
				 	:date => "12/05/2013", 
				 	:description => "Internet monthly", 
				 	:amount => "700000", 
				 	:vat => "40000",
				 	:provider => '',
				 	:provider_vat_regno => '',
				 	:justification => ''
				}
			}
		put :update, params

		assigns(:expense).should be_valid

		updated_expense = Expense.find('1')
		updated_expense.should_not be_nil

		updated_expense.amount.should eq(700000)
		updated_expense.vat.should eq(40000)
		updated_expense.provider.should eq('')
		updated_expense.provider_vat_regno.should eq('')
	end

	test 'should not update expense without required information' do
		login_user 1
		params = 
			{
				:id => 1,
				:expense => {
					:reference => "", 
				 	:date => "12/05/2013", 
				 	:description => "Internet monthly", 
				 	:amount => "", 
				 	:vat => "",
				 	:provider => 'FPT',
				 	:provider_vat_regno => 'FPT001',
				 	:justification => 'Nothing'
				}
			}
		put :update, params

		assigns(:expense).should_not be_valid

		updated_expense = Expense.find('1')
		updated_expense.should_not be_nil

		updated_expense.reference.should eq('1')
		updated_expense.amount.should eq(1000)
		updated_expense.vat.should eq(15)
	end

end