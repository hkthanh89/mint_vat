class HomeController < ApplicationController
  skip_before_filter :require_user

  def index
    setup
    if current_user
      redirect_home
    else
      render :layout => false
    end
  end

  def signup
    @sign_up = SignUp.new(params[:sign_up])
    if @sign_up.valid?
      user = User.new(
          :email => @sign_up.email,
          :password => @sign_up.password,
          :client_ip => request.remote_ip
      )
      if user.save
        welcome user
      else
        show_errors :errors, user.errors
      end
    else
      show_errors :errors, @sign_up.errors
    end
  end

  def login
    @login = Login.new(params[:login])
    if @login.valid?
      user = User.authenticate @login.email, @login.password
      if user
        welcome user
      else
        @login.errors.add :base, 'Email and/or password were not correct'
        show_errors :login_errors, @login.errors
      end
    else
      show_errors :login_errors, @login.errors
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to :action => :index
  end

  private
  def setup
    @sign_up = SignUp.new unless @sign_up
    @login = Login.new unless @login
  end

  def welcome(user)
    session[:user_id] = user.id
    redirect_home
  end

  def redirect_home
    redirect_to :controller => :welcome, :action => :index
  end

  def show_errors(errors_name, errors)
    flash[errors_name] = errors
    setup
    render :index, :layout => false
  end
end
