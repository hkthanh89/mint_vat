class CompaniesController < ApplicationController
  def index
    @company = Company.find_by_user_id(current_user.id) || Company.new
  end

  def modify
    @company = Company.find_by_user_id(current_user.id)

    if @company.nil? ? create(params) : update(params)
      redirect_to :action => :index, notice: 'Company was successfully updated.'
    else
      render :action => :index
    end
  end

  private
  def create(params)
    @company = Company.new(params[:company])
    @company.user_id = current_user.id
    @company.save
  end

  def update(params)
    @company.update_attributes(params[:company])
  end
end