class InvoicesController < ApplicationController
  def index
    @invoices = Invoice.order('date DESC').find_all_by_user_id(current_user.id)
  end

  def new
    @invoice = Invoice.new

    company = Company.find_by_user_id(current_user.id)
    if company
      @invoice.self_name = company.name
      @invoice.self_address_1 = company.address_1
      @invoice.self_address_2 = company.address_2
      @invoice.self_town = company.town
      @invoice.self_postcode = company.postcode
      @invoice.self_county = company.county
    end
    @invoice.number = Invoice.next_number current_user.id
    @invoice.date = Time.now.strftime('%d/%m/%Y')
    @invoice.print_payment_details = true

    find_or_create_client_by_invoice_client_data @invoice
    create_clients_json
  end

  def create
    @invoice = Invoice.new(params[:invoice])
    @invoice.invoice_rows = []
    add_new_rows params[:new_invoice_rows]

    @invoice.user_id = session[:user_id]
    @invoice.calculate_totals

    update_company_details
    create_or_update_client_and_invoice_client_data

    if @invoice.save
      redirect_to :action => :index, notice: 'Invoice was successfully created.'
    else
      render :action => :new
    end
  end

  def edit
    @invoice = Invoice.find_by_id_and_user_id(params[:id], current_user.id) || not_found

    find_or_create_client_by_invoice_client_data @invoice
    create_clients_json
  end

  def update
    @invoice = Invoice.find_by_id_and_user_id(params[:id], current_user.id) || not_found

    @invoice.assign_attributes params[:invoice]
    update_rows params[:invoice_rows]
    add_new_rows params[:new_invoice_rows]
    @invoice.calculate_totals

    update_company_details
    create_or_update_client_and_invoice_client_data

    if @invoice.save
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def pdf
    @invoice = Invoice.find_by_id_and_user_id(params[:id], current_user.id) || not_found
    pdf = InvoicePdfGenerator.new(@invoice)
    send_data pdf.render_pdf, {
        :filename => "invoice_#{@invoice.number}.pdf",
        :type => 'application/pdf',
        :disposition => 'attachment'
    }
  end

  def self.clients_json(clients)
    clients_json = []
    clients.each do |c|
      clients_json << {
          :value => c.id,
          :label => c.name,
          :vat_code => c.vat_code,
          :address_1 => c.address_1,
          :address_2 => c.address_2,
          :town => c.town,
          :postcode => c.postcode,
          :county => c.county
      }
    end

    clients_json = clients_json.to_json
  end

  private
  def add_new_rows(new_invoice_rows)
    new_invoice_rows.each do |row|
      @invoice.invoice_rows.build(row).calculate_net_price
    end unless new_invoice_rows.nil?
  end

  def update_rows(invoice_rows)
    invoice_rows ||= {}
    @invoice.invoice_rows.each do |row|
      updated_row = invoice_rows[row.id.to_s]
      if updated_row.nil?
        row.delete
      else
        row.assign_attributes updated_row
        row.calculate_net_price
      end
    end
  end

  def create_or_update_client_and_invoice_client_data
    client_id = params[:client_id]
    @client = Client.find_by_id_and_user_id(client_id, current_user.id)
    if @client
      @client.assign_attributes params[:client]
    else
      @client = Client.new(params[:client])
      @client.user_id = current_user.id
    end

    create_clients_json

    if @client.save
      @invoice.update_client_data @client
    end
  end

  def update_company_details
    new_company_attributes = {:name => params[:invoice][:self_name], :address_1 => params[:invoice][:self_address_1], :address_2 => params[:invoice][:self_address_2], :town => params[:invoice][:self_town], :postcode => params[:invoice][:self_postcode]}
    company = Company.find_by_user_id(current_user.id)
    if company
      company.assign_attributes(new_company_attributes)
    else
      company = Company.new(new_company_attributes)
      company.user_id = current_user.id
    end
    company.save!
  end

  def create_clients_json
    clients = Client.find_all_by_user_id(current_user.id)
    @clients_json = InvoicesController.clients_json(clients)
  end

  def find_or_create_client_by_invoice_client_data(invoice)
    clients = Client.find_all_by_user_id(current_user.id)
    @client = clients.detect{|c| c.name == invoice.client_name}

    if not @client
      @client = Client.new
      @client.update_from_invoice invoice
    end
  end

end