class ExpensesController < ApplicationController
  
  def index
    @expenses = Expense.order('date DESC').find_all_by_user_id(current_user.id)
  end

  def new
  	@expense = Expense.new

    @expense.reference = Expense.next_number current_user.id
    @expense.date = Time.now.strftime('%d/%m/%Y')
  end

  def create
    @expense = Expense.new(params[:expense])

    @expense.user_id = session[:user_id]

    if @expense.save
      redirect_to expenses_url
    else
      render 'new'
    end
  end

  def edit
    @expense = Expense.find_by_id_and_user_id(params[:id], current_user.id) || not_found
  end

  def update
    @expense = Expense.find_by_id_and_user_id(params[:id], current_user.id) || not_found
  
    @expense.assign_attributes params[:expense]

    if @expense.save
      redirect_to expenses_url
    else
      render 'edit'
    end
  end

end
