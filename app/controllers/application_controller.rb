class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :require_user
  helper_method :current_user

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def require_user
    redirect_to :root unless current_user unless @dont_require_user
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end
