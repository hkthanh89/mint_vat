class WelcomeController < ApplicationController

  def index
    redirect_to :controller => :invoices, :action => :index
  end

end