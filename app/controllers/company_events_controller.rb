class CompanyEventsController < ApplicationController

  def index
    company_id = Company.where(:user_id => session[:user_id]).first.id
    @company_events = CompanyEvent.get_events(company_id) 

  	@company_event = CompanyEvent.new
    @company_event.date = Date.today

    @current_year = CompanyEvent.last == nil ? Date.today.year : CompanyEvent.last.created_at.year
  end

  def create
  	@company_event = CompanyEvent.new(params[:company_event])
  	@company_event.company_id = Company.where(:user_id => session[:user_id]).first.id
    @company_event.date = "01-" + params[:company_event][:date]

    respond_to do |format|
      if @company_event.save
        format.html { redirect_to company_events_url }
        format.js 
      else
        format.html { render :action => "index" }
      end
    end
  end

  def edit
    company_id = Company.where(:user_id => session[:user_id]).first.id
    @company_event = CompanyEvent.find_by_id_and_company_id(params[:id], company_id) || not_found

    respond_to do |format|
      format.js
    end
  end

  def update
    company_id = Company.where(:user_id => session[:user_id]).first.id
    @company_event = CompanyEvent.find_by_id_and_company_id(params[:id], company_id) || not_found

    @company_event.assign_attributes params[:company_event]
    @company_event.date = "01-" + params[:company_event][:date]
    
    respond_to do |format|
      if @company_event.save
        format.html { redirect_to company_events_url }
        format.js
      else
        format.html { render :action => 'index' }
      end
    end
  end

  def destroy
    company_id = Company.where(:user_id => session[:user_id]).first.id
    @company_event = CompanyEvent.find_by_id_and_company_id(params[:id], company_id) || not_found
    @company_event.destroy()

    respond_to do |format|
      format.html { redirect_to company_events_url }
      format.js
    end
  end

end
