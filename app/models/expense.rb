class Expense < ActiveRecord::Base
  attr_accessible :amount,
  								:date, 
  								:description, 
  								:justification, 
  								:provider, 
  								:provider_vat_regno, 
  								:reference, 
  								:vat

  belongs_to :user

  validates :amount,
            :date,
            :description,
            :reference,
            :user,
            :vat, :presence => true

  validates :amount, :vat, :numericality => { :greater_than_or_equal_to => 0 }

  def self.next_number(current_user_id)
    last_number = Expense.maximum('0+reference', :conditions => ["user_id = #{current_user_id}"])
    (last_number.to_i + 1).to_s unless last_number == 0
  end
end
