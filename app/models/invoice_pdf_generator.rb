#coding: utf-8
require 'prawn'

class InvoicePdfGenerator
  include ActionView::Helpers::NumberHelper

  FONTS_LOCATION = Rails.root.join('app/assets/fonts/')
  HEADING_COLOR = '284677'

  attr_reader :pdf

  def initialize(invoice)
    @invoice = invoice
    @company = Company.find_by_user_id(@invoice.user_id)
    @pdf = Prawn::Document.new :page_size => 'A4', :margin => 50, :top_margin => 75

    load_fonts

    @pdf.fill_color = HEADING_COLOR
    @pdf.font('Calibri', :size => 24, :style => :bold) do
      @pdf.text 'INVOICE'
    end

    @pdf.font('Cambria')
    create_provider_customer_table
    create_invoice_no_and_date
    create_invoice_table
    create_payment
  end

  def write_pdf_file
    @pdf.render_file '/tmp/inv.pdf'
  end

  def render_pdf
    @pdf.render
  end

  private

  def create_provider_customer_table
    table_data = []
    self_details = []
    customer_details = []

    add_unless_blank(self_details, "<color rgb='#{HEADING_COLOR}'><b><i>From:</u></i></b></color>")
    add_unless_blank(self_details, @invoice.self_name, "<b>#{@invoice.self_name}</b>")
    add_unless_blank(self_details, @invoice.self_address_1)
    add_unless_blank(self_details, @invoice.self_address_2)
    add_unless_blank(self_details, @invoice.self_town)
    add_unless_blank(self_details, @invoice.self_county)
    add_unless_blank(self_details, @invoice.self_postcode)
    # TODO uncomment the following when the company will have company number
    #add_unless_blank(self_details, @company.self_company_no, "<i>Company no</i>: #{@invoice.self_company_no}")
    add_unless_blank(self_details, @company.vat_code, "<i>VAT no</i>: #{@company.vat_code}") unless @company.nil?

    add_unless_blank(customer_details, "<color rgb='#{HEADING_COLOR}'><b><i>To:</u></i></b></color>")
    add_unless_blank(customer_details, @invoice.client_name, "<b>#{@invoice.client_name}</b>")
    add_unless_blank(customer_details, @invoice.client_address_1)
    add_unless_blank(customer_details, @invoice.client_address_2)
    add_unless_blank(customer_details, @invoice.client_town)
    add_unless_blank(customer_details, @invoice.client_county)
    add_unless_blank(customer_details, @invoice.client_postcode)
    add_unless_blank(customer_details, @invoice.client_company_no, "<i>Company no</i>: #{@invoice.client_company_no}")

    [self_details.count, customer_details.count].max.times do |ix|
      table_data << [self_details[ix], customer_details[ix]]
    end

    @pdf.move_down 10
    @pdf.fill_color = "000000"
    @pdf.table(table_data,
               :column_widths => [@pdf.bounds.width*3/5, @pdf.bounds.width*2/5],
               :cell_style => {
                   :border_width => 0,
                   :inline_format => true,
                   :padding => 3
               }
    )
  end

  def create_invoice_no_and_date
    table_data = []

    unless @invoice.number.to_s == ''
      table_data << ['<b>Invoice no: </b>', @invoice.number]
    end
    unless @invoice.date.to_s == ''
      table_data << ['<b>Invoice date: </b>', @invoice.date.strftime('%d/%m/%Y')]
    end

    unless table_data == []
      @pdf.move_down 12
      @pdf.table(table_data,
                 :width => @pdf.bounds.width,
                 :column_widths => [@pdf.bounds.width/2, @pdf.bounds.width/2],
                 :cell_style => {
                     :border_width => 0,
                     :inline_format => true,
                     :padding => 3,
                     :align => :left
                 }
      ) do
        column(0).style :align => :right
      end
    end
  end

  def create_invoice_table
    table_data = [['<b>Description</b>',
                   '<b>Quantity</b>',
                   '<b>Unit Price</b>',
                   '<b>Net Price</b>']]

    @invoice.invoice_rows.each do |row|
      table_data << ([] << "#{row.name} <font size='8'>#{row.details || ''}</font>" <<
        "#{number_with_precision(row.quantity, :precision => 2, :strip_insignificant_zeros => true)} #{row.unit || ''}" <<
        number_to_currency(row.unit_price, :unit => '£', :separator => '.', :delimiter => ',') <<
        number_to_currency(row.net_price, :unit => '£', :separator => '.', :delimiter => ','))
    end
    rows = @invoice.invoice_rows.count

    table_data << ([] << {:content => ' '} <<
        {:content => ' '} <<
        'Net Amount' <<
        number_to_currency(@invoice.inv_total, :unit => '£', :separator => '.', :delimiter => ','))
    table_data << ([] << {:content => ' '} <<
        {:content => ' '} <<
        "VAT @ #{number_with_precision(@invoice.vat_rate, :precision => 2, :strip_insignificant_zeros => true)}%" <<
        number_to_currency(@invoice.inv_vat_total, :unit => '£', :separator => '.', :delimiter => ','))
    table_data << ([] << {:content => ' '} <<
        {:content => ' '} <<
        '<b>Invoice Total</b>' <<
        "<b>#{number_to_currency(@invoice.inv_grand_total, :unit => '£', :separator => '.', :delimiter => ',')}</b>")

    @pdf.move_down 10
    @pdf.table(table_data, {
        :width => @pdf.bounds.width,
        :column_widths => [@pdf.bounds.width*4.5/10, @pdf.bounds.width*1.5/10, @pdf.bounds.width*2/10, @pdf.bounds.width*2/10],
        :header => true,
        :cell_style => {
            :align => :right,
            :inline_format => true,
            :padding_right => 8
        }
    }) do
      column(0).style :align => :left
      row(rows+1..rows+2).column(0).borders = [:left]
      row(rows+1..rows+2).column(1).borders = []
      row(rows+3).column(0).borders = [:left, :bottom]
      row(rows+3).column(1).borders = [:bottom]
      if rows > 1
        row(1).borders = [:top, :left, :right]
        (2..rows).each do |crt_row|
          row(crt_row).borders = [:left, :right]
        end
      end
      row(rows).height = 200
      row(rows).borders = [:bottom, :left, :right]
    end
  end

  def create_payment
    if @invoice.print_payment_details
      unless @company.nil? || @company.bank.to_s.strip == ''

        table_data = [['Bank:', @company.bank]]
        unless @company.account.to_s == ''
          table_data << ['Account no:', @company.account]
        end
        unless @company.sort_code.to_s == ''
          table_data << ['Sort code:', @company.sort_code]
        end

        unless @invoice.due_date.to_s == ''
          table_data << ['Please pay by:', @invoice.due_date]
        end

        @pdf.move_down 20
        @pdf.formatted_text [{:text => 'Funds should be transferred to the following account:', :styles => [:italic]}]
        @pdf.move_down 5
        @pdf.table(table_data, {
            :column_widths => [100, @pdf.bounds.width - 100],
            :cell_style => {
                :inline_format => true,
                :padding => 1,
                :border_width => 0
            }
        })
      end
    end
  end

  def load_fonts
    @pdf.font_families.update('Calibri' => {
        :normal => "#{FONTS_LOCATION}/Calibri.ttf",
        :italic => "#{FONTS_LOCATION}/Calibri Italic.ttf",
        :bold => "#{FONTS_LOCATION}/Calibri Bold.ttf",
        :bold_italic => "#{FONTS_LOCATION}/Calibri Bold Italic.ttf"
    })
    @pdf.font_families.update('Cambria' => {
        :normal => "#{FONTS_LOCATION}/Cambria.ttf",
        :bold => "#{FONTS_LOCATION}/Cambria Bold.ttf",
        :italic => "#{FONTS_LOCATION}/Cambria Italic.ttf",
        :bold_italic => "#{FONTS_LOCATION}/Cambria Bold Italic.ttf",
    })
  end

  def add_unless_blank(ary, element, text_fmt=nil)
    unless element.to_s == ''
      ary << (text_fmt.to_s == '' ? element : text_fmt)
    end
  end

end