class CompanyEvent < ActiveRecord::Base
  attr_accessible :date, :event_id, :flat_rate_1, :flat_rate_2, :new_flat_rate

  belongs_to :company
  belongs_to :event

  validates :company_id,
  					:date,
  					:event_id,
  					:presence => true

  validates :new_flat_rate, 
  					:flat_rate_1, 
  					:flat_rate_2,
  					:numericality => { :greater_than_or_equal_to => 0 }

  validates :date, :format => { :with => /^(\d{4}-(0[1-9]|1[012])-(01))$/,
            :message => "is not valid"}


  def self.get_events(company_id)
    self.includes(:event).where(:company_id => company_id).order("date DESC").group_by { |ce| ce.date.strftime("%Y") }
  end

end
