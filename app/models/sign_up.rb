class SignUp
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :client_ip, :username, :email, :password, :password_confirmation

  validates :username, :length => {:is => 0}
  validates :password, :confirmation => true

  def initialize(attributes = {})
    @attributes = attributes
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def read_attribute_for_validation(key)
    @attributes[key]
  end

end