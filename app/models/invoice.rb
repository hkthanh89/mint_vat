class Invoice < ActiveRecord::Base

  attr_accessible :number,
                  :date,
                  :vat_rate,

                  :client_name,
                  :client_company_no,
                  :client_address_1,
                  :client_address_2,
                  :client_town,
                  :client_postcode,
                  :client_county,
                  :client_vat_code,

                  :self_name,
                  :self_address_1,
                  :self_address_2,
                  :self_town,
                  :self_postcode,
                  :self_county,

                  :due_date,
                  :self_bank,
                  :self_account,
                  :self_sort_code,

                  :inv_total,
                  :inv_vat_total,
                  :inv_grand_total,

                  :print_payment_details

  belongs_to :user
  has_many :invoice_rows, :autosave => true

  validates :number, :presence => true
  validates :date, :presence => true
  validates :vat_rate, :presence => true, :numericality => { :greater_than_or_equal_to => 0, :less_than => 100 }
  validates :client_name, :presence => true
  validates :self_name, :presence => true
  validates :invoice_rows, :length => {:minimum => 1}
  validates :user, :presence => true

  validates :inv_total, :presence => true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :inv_vat_total, :presence => true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :inv_grand_total, :presence => true, :numericality => { :greater_than_or_equal_to => 0 }

  validates_inclusion_of :print_payment_details, :in => [true, false]

  def calculate_totals
    calculate_inv_total
    calculate_vat_total
    calculate_inv_grand_total
  end

  def update_client_data(client)
    self.client_name = client.name
    self.client_vat_code = client.vat_code
    self.client_address_1 = client.address_1
    self.client_address_2 = client.address_2
    self.client_town = client.town
    self.client_postcode = client.postcode
    self.client_county = client.county
  end

  def self.next_number(current_user_id)
    last_number = Invoice.maximum('0+number', :conditions => ["user_id = #{current_user_id}"])
    (last_number.to_i + 1).to_s unless last_number == 0
  end

  private
  def calculate_inv_total
    self.inv_total = 0.0
    invoice_rows.each do |row|
      self.inv_total += row.net_price.to_f
    end
  end

  def calculate_vat_total
    unless self.vat_rate.nil?
      self.inv_vat_total = self.vat_rate.to_d / 100 * self.inv_total
    end
  end

  def calculate_inv_grand_total
    self.inv_grand_total = self.inv_vat_total + self.inv_total
  end

  end