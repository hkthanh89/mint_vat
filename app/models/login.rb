class Login
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :email, :password

  validates :email, :presence => true
  validates :password, :presence => true

  def initialize(attributes = {})
    @attributes = attributes
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def read_attribute_for_validation(key)
    @attributes[key]
  end

end