class Company < ActiveRecord::Base

  attr_accessible :name,
                  :vat_code,
                  :address_1,
                  :address_2,
                  :town,
                  :postcode,
                  :county,
                  :bank,
                  :account,
                  :sort_code

  belongs_to :user

  has_many :company_events
  has_many :events, :through => :company_events

  validates :name, :presence => true
  validates :user, :presence => true

end