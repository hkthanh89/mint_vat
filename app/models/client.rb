class Client < ActiveRecord::Base

  attr_accessible :name,
                  :vat_code,
                  :address_1,
                  :address_2,
                  :town,
                  :postcode,
                  :county

  belongs_to :user

  validates :name, :presence => true

  validates :user, :presence => true

  def update_from_invoice(invoice)
    self.name = invoice.client_name
    self.vat_code = invoice.client_vat_code
    self.address_1 = invoice.client_address_1
    self.address_2 = invoice.client_address_2
    self.town = invoice.client_town
    self.postcode = invoice.client_postcode
    self.county = invoice.client_county
  end

end