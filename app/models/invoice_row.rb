class InvoiceRow < ActiveRecord::Base

  attr_accessible :name,
                  :details,
                  :quantity,
                  :unit,
                  :unit_price,
                  :net_price

  belongs_to :invoice, :autosave => true

  validates :name, :presence => true
  validates :quantity, :presence => true, :numericality => { :greater_than => 0 }
  validates :unit_price, :presence => true, :numericality => { :greater_than => 0 }
  validates :net_price, :presence => true, :numericality => { :greater_than => 0 }

  def calculate_net_price
    self.net_price = self.unit_price.to_f * self.quantity.to_f
  end

end