class User < ActiveRecord::Base
  attr_accessible :client_ip, :email, :password

  has_secure_password

  validates :email, :presence => true, :uniqueness => true
  validates :password, :presence => true
  validates :client_ip, :presence => true

  def self.authenticate(email, password)
    find_by_email(email).try(:authenticate, password)
  end

end
