
class @ExpensePage

  @start: ->
    new ExpensePage()

  constructor: ->
    @install_listeners()

  install_listeners: ->
    $("#submitExpense").click (e) =>
      e.preventDefault()
      @validate_form()

  validate_form: ->

    isValid = true
    error_messages = []

    inputs = $('input#expense_reference, input#expense_date, input#expense_description, input#expense_amount, input#expense_vat')
    for input in inputs
      value = $(input).val().replace /^\s+|\s+$/g, ""
      if value is ''
        isValid = false
        error_messages.push($(input).attr('placeholder') + " can't be blank")
        $(input).addClass('input_error')
      else
        $(input).removeClass('input_error')

    inputs_number = $('input#expense_amount, input#expense_vat')
    for input_number in inputs_number
      if $.isNumeric($(input_number).val())
        if $(input_number).val() < 0
          isValid = false
          error_messages.push($(input_number).attr('placeholder') + " must be greater than or equal to 0")
          $(input_number).addClass('input_error')
      else
        isValid = false
        error_messages.push($(input_number).attr('placeholder') + " must be a number")
        $(input_number).addClass('input_error')

    if isValid is false
      error = if error_messages.length > 1 then " errors:" else " error:"
      div_error = @create_div_error(error_messages.length, error)
      $('.perforated').before(div_error)
      @display_errors(error_messages)
    else
      $('form').submit()

  display_errors: (error_messages) ->
    ul = $("#error_explanation_expense").find("ul")
    ul.empty()
    for message in error_messages
      li = $("<li>" + message + "</li>")
      ul.append(li)

  create_div_error: (error_messages_length, error) ->
    div_error = $("#error_explanation_expense").show()
    div_error.find("strong").text("Expense saving faild because of #{error_messages_length} #{error}")
    div_error