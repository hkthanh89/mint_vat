class @CompanyPage

  @start: ->
    new CompanyPage()

  constructor: ->
    @install_listeners()
    @hide_success_message()

  install_listeners: ->

  hide_success_message: ->
    $('#success-explanation').delay(3000).slideUp()