class @HomePage

  login_visible = false

  @start: ->
    new HomePage()

  constructor: ->
    @ensure_right_form_visible()
    @install_listeners()

  ensure_right_form_visible: ->
    if (@is_login_url())
      $('#signup').hide()
      $('#login').show()
      login_visible = true
      $('#login_button').html 'Create account'

  is_login_url: ->
    str = window.location.href
    suffix = 'login'
    return str.indexOf(suffix, str.length - suffix.length) isnt -1

  install_listeners: ->
    $('#login_button').click (e) =>
      e.preventDefault()
      @show_login() unless login_visible
      @hide_login() if login_visible

  show_login: ->
    $('#signup').animate({'margin-left': '-600px'}, 400, ->
      $('#signup').hide()
      $('#login').css('margin-right', '-800px')
      $('#login').show()
      $('#login').animate({'margin-right': '290px'}, 400, ->
        $('#login').css('margin-right': '0 auto')
        $('#login_button').html 'Create account'
        $('#login_email').focus()
        login_visible = true
      )
    )

  hide_login: ->
    $('#login').animate({'margin-right': '-800px'}, 400, ->
      $('#login').hide()
      $('#signup').css('margin-left': '-600px')
      $('#signup').show()
      $('#signup').animate({'margin-left': '290px'}, 400, ->
        $('#signup').css('margin-left': '0 auto')
        $('#login_button').html 'Login'
        $('#sign_up_email').focus()
        login_visible = false
      )
    )

