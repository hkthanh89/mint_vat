class @InvoicePage

  @start: ->
    new InvoicePage()

  constructor: ->
    @install_listeners()
    @ensure_at_least_one_row()
    @display_appropriate_self_details_link()
    @install_tooltips()

  install_listeners: ->
    $('.invoice-row INPUT, #invoice_vat_rate').change (e) =>
      @compute_row_totals()
    $('#add-invoice-row-button').click (e) =>
      @add_invoice_row()
    $('.delete-row').click (e) =>
      @remove_row @find_row_id $(e.currentTarget)
    $('#invoice_client .show-more').click (e) =>
      e.preventDefault()
      @show_more_client()
    $('#invoice_client .show-less').click (e) =>
      e.preventDefault()
      @show_less_client()
    $('#invoice_self .show-more').click (e) =>
      e.preventDefault()
      @show_more_self()
    $('#invoice_self .show-less').click (e) =>
      e.preventDefault()
      @show_less_self()
    $('#invoice_self_name').change (e) =>
      @display_appropriate_self_details_link()

  install_tooltips: ->
    if @show_tooltips
      $('#add-invoice-row-button').tooltip()
      $('.delete-row').tooltip()
      $('.row-name, .row-quantity, .row-unit, .row-unit-price').tooltip()

  compute_row_totals: ->
    invoice_total = 0.0
    $('.invoice-row:visible').each (index, element) =>
      quantity = $(element).find('.row-quantity').val()
      unit_price = $(element).find('.row-unit-price').val()
      net_price = accounting.formatMoney((quantity * unit_price), "£", 2)
      $(element).find('.row-net-price').html(net_price)
      $(element).find('TD').first().find('.row-num').html(index + 1)
      invoice_total += (quantity * unit_price)
    invoice_vat_total = invoice_total * $('#invoice_vat_rate').val() / 100
    invoice_grand_total = invoice_total + invoice_vat_total
    $('#invoice-vat-total').html(accounting.formatMoney(invoice_vat_total, "£", 2))
    $('#invoice-grand-total').html(accounting.formatMoney(invoice_grand_total, "£", 2))
    $('#invoice_total_vat_rate').html($('#invoice_vat_rate').val())

  add_invoice_row: (options) ->
    new_invoice_row = $('#new-invoice-row')
    last_invoice_row_id = parseInt $('.invoice-row:visible').last().find('TD').first().find('.row-num').html()
    new_row_id = 1 + (if isNaN(last_invoice_row_id) then 0 else last_invoice_row_id)
    $('#invoice-rows TABLE TBODY').append(new_invoice_row.clone().attr('data-row', new_row_id).show())
    @create_new_row(new_row_id, options)
    @compute_row_totals()

  create_new_row: (new_row_id, options) ->
    new_invoice_row = $('#invoice-rows .invoice-row').last()
    new_invoice_row.attr('id', "new-invoice-row-#{new_row_id}")
    new_invoice_row.find('INPUT').change (e) =>
      @compute_row_totals()
    new_invoice_row.find('INPUT[type="text"]').val('')
    new_invoice_row.find('INPUT[type="text"]').prop('disabled', false);
    new_invoice_row.find('BUTTON').tooltip() if @show_tooltips
    new_invoice_row.find('BUTTON').click (e) =>
      @remove_row(new_row_id)
    new_invoice_row.find('INPUT[type="text"]').tooltip() if new_row_id == 1 and @show_tooltips
    new_invoice_row.find('.row-name').focus() unless options and options.no_focus

  remove_row: (row_id) ->
    $("#new-invoice-row-#{row_id} BUTTON, #invoice-row-#{row_id} BUTTON").tooltip('hide')
    $("#new-invoice-row-#{row_id}, #invoice-row-#{row_id}").remove()
    @compute_row_totals()

  ensure_at_least_one_row: ->
    @add_invoice_row(no_focus: true) unless $('.invoice-row:visible').length

  find_row_id: (remove_button) ->
    tr_id = remove_button.parent().parent().attr('id')
    tr_id.split('_').pop()

  display_appropriate_self_details_link: ->
    if @requires_self_details_entry()
      $('#no-details-link').show()
      $('#check-details-link').hide()
    else
      $('#check-details-link').show()
      $('#no-details-link').hide()

  requires_self_details_entry: ->
    $('#invoice_self_name').val().length is 0
