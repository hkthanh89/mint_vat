
class @InvoiceClientsAutocomplete

  @start: ->
    new InvoiceClientsAutocomplete()

  constructor: ->

  clearClientInfo: (client_name_field)->
    if(client_name_field.is($( "#client_name" )))
      $( "#client_name_invoice_page").val( $( "#client_name" ).val() )
    if(client_name_field.is($( "#client_name_invoice_page" )))
      $( "#client_name").val( $( "#client_name_invoice_page" ).val() )

    $( "#client_id" ).val( "0" )
    $( "#client_vat_code" ).val( "" )
    $( "#client_address_1" ).val( "" )
    $( "#client_address_2" ).val( "" )
    $( "#client_town" ).val( "" )
    $( "#client_postcode" ).val( "" )
    $( "#client_county" ).val( "" )

  populateClientInfo: (clientJSON) ->
    $( "#client_name" ).val( clientJSON.label )
    $( "#client_name_invoice_page" ).val( clientJSON.label )
    $( "#client_id" ).val( clientJSON.value )
    $( "#client_vat_code" ).val( clientJSON.vat_code )
    $( "#client_address_1" ).val( clientJSON.address_1 )
    $( "#client_address_2" ).val( clientJSON.address_2 )
    $( "#client_town" ).val( clientJSON.town )
    $( "#client_postcode" ).val( clientJSON.postcode )
    $( "#client_county" ).val( clientJSON.county )

  autocompleteClientName: (client_name_field, clients) ->
    autocomplete = this
    client_name_field.autocomplete (
      minLength: 0,
      source: clients,
      focus: ( event, ui ) ->
        client_name_field.val( ui.item.label )
        return false
      select: ( event, ui ) ->
        autocomplete.populateClientInfo( ui.item )
        return false
      change: ( event, ui ) ->
        if (ui.item == null)
          autocomplete.clearClientInfo(client_name_field)
        return false
    )

  installAutocompleteListeners: (clients) ->
    autocomplete = this
    autocomplete.autocompleteClientName($( "#client_name" ), clients)
    autocomplete.autocompleteClientName($( "#client_name_invoice_page" ), clients)


