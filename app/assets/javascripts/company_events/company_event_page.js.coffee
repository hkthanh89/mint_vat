
class @CompanyEventPage
  
  @start: ->
    new CompanyEventPage()

  constructor: ->
    @install_listeners()

  install_listeners: ->

    $("#newModal").on "show", =>
      @set_datepicker("#newModal")
      @event_change("#newModal")

    $("#destroyModal").on "show", ->
      $("#editModal").modal('hide')

    $("#close_destroy_modal").on 'click', ->
      $("#editModal").modal('show')

    $("#add_event").click (e) =>
      e.preventDefault()
      @hide_optional_fields()
      $("#newModal").modal('show')

    $("#submitCompanyEvent").click (e) =>
      e.preventDefault()
      @validate_form("#newModal")

    $("#editCompanyEvent").click (e) =>
      e.preventDefault()
      @validate_form("#editModal")

  create_div_error: (error_messages_length, error, current_modal) ->
    div_error = $("#{current_modal} #error_explanation_company_event").show()
    div_error.find("strong").text("Company Event saving faild because of #{error_messages_length} #{error}")
    div_error

  display_errors: (error_messages, current_modal) ->
    ul = $("#{current_modal} #error_explanation_company_event").find("ul")
    ul.empty()
    for message in error_messages
      li = $("<li>" + message + "</li>")
      ul.append(li)

  event_change: (current_modal) ->
    $("#{current_modal} #company_event_event_id").change =>
      @reset_optional_fields(current_modal)
      @hide_optional_fields(current_modal)
      $('.input_error').removeClass('input_error')
      $("#{current_modal} #error_explanation_company_event").hide()

      value_select = $("#{current_modal} #company_event_event_id").val()

      switch value_select
        when '2'
          $("#{current_modal} #flat_rate_1").show()
          $("#{current_modal} #flat_rate_2").show()
        when '4'
          $("#{current_modal} #new_flat_rate").show()

  hide_optional_fields: (current_modal) ->
    $("#{current_modal} #new_flat_rate").hide()
    $("#{current_modal} #flat_rate_1").hide()
    $("#{current_modal} #flat_rate_2").hide()

  reset_optional_fields: (current_modal) ->
    $("#{current_modal} #company_event_new_flat_rate").val(0)
    $("#{current_modal} #company_event_flat_rate_1").val(0)
    $("#{current_modal} #company_event_flat_rate_2").val(0)

  set_datepicker: (current_modal) ->
    $("#{current_modal} #company_event_date").datepicker
      format: "mm-yyyy"
      viewMode: "months"
      minViewMode: "months"

  validate_form: (current_modal) =>

    isValid = true
    error_messages = []

    event = $("#{current_modal} #company_event_event_id")
    value = event.val().replace /^\s+|\s+$/g, ""

    if value is ''
      isValid = false
      event.addClass('input_error')
      error_messages.push("Event can't be blank")

    inputs_number = $("#{current_modal} input#company_event_new_flat_rate, #{current_modal} input#company_event_flat_rate_1, #{current_modal} input#company_event_flat_rate_2")
    for input_number in inputs_number
      input = $(input_number)

      if $.isNumeric(input.val())
        if input.val() < 0
          isValid = false
          input.addClass('input_error')
          error_messages.push(input.attr('placeholder') + " must be greater than or equal to 0")
      else
        isValid = false
        input.addClass('input_error')
        error_messages.push(input.attr('placeholder') + " must be a number")
        
    if isValid is false
      error = if error_messages.length > 1 then " errors:" else " error:"
      div_error = @create_div_error(error_messages.length, error, current_modal)
      @display_errors(error_messages, current_modal)
    else
      if current_modal is "#newModal"
        $("#{current_modal} form[id='new_company_event']").submit()
        @hide_optional_fields("#newModal")
      else if current_modal is "#editModal"
        $("#{current_modal} form[id^='edit_company_event']").submit()
