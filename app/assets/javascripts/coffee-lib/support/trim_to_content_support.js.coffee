class @TrimToContentSupport

  BUFFER = 4

  @trim_all_to_content: ->
    trimToContentSupport = new TrimToContentSupport()
    $('.trim-to-content').each (index, element) =>
      trimToContentSupport.apply_for(element)

  apply_for: (element) ->
    original_width = $(element).width()
    @trim(element, original_width)
    $(element).focus () =>
      @trim(element, original_width)
    $(element).blur () =>
      @trim(element, original_width)
    $(element).keypress (e) =>
      if e.which isnt 0 and e.charCode isnt 0
        extra_char = String.fromCharCode(e.keyCode | e.charCode)
      else
        extra_char = ''
      @trim(element, original_width, extra_char)
    $(element).keyup (e) =>
      if $(element).val().length is 0
        @trim(element, original_width)

  trim: (element, original_width, extra_char='') ->
    if $(element).val().length > 0 or extra_char.length > 0
      $(element).width(@calculate_trimmed_width(element, extra_char))
    else
      $(element).width(original_width)

  calculate_trimmed_width: (element, extra_char) ->
    text = $(element).val() + extra_char
    trim_span = $("<span class='input' style='display:none'>#{text}</span>")
    trim_span.appendTo($(element).parent())
    trimmed_width = trim_span.width()
    trim_span.remove()
    return trimmed_width + BUFFER
