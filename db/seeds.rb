# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

event_names = [ "Registered for VAT", 
								"Applied for the Flat Rate scheme",
								"De-registered from the Flat Rate scheme",
								"The Flat Rate changed",
								"De-registered from VAT"
							]

event_names.each do |name|
	Event.find_or_create_by_name name
end