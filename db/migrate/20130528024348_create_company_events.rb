class CreateCompanyEvents < ActiveRecord::Migration
  def change
    create_table :company_events do |t|
      t.integer :company_id, :null => false
      t.integer :event_id, :null => false
      t.date :date, :null => false
      t.decimal :new_flat_rate, :null => false, :default => 0, :precision => 10, :scale => 2
      t.decimal :flat_rate_1, :null => false, :default => 0, :precision => 10, :scale => 2
      t.decimal :flat_rate_2, :null => false, :default => 0, :precision => 10, :scale => 2

      t.timestamps
    end

    add_foreign_key :company_events, :companies
    add_foreign_key :company_events, :events
  end
end

