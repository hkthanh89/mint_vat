class CreateInvoice < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :number, :null => false
      t.date :date, :null => false
      t.decimal :vat_rate, :null => false, :default => 0
      t.date :due_date

      t.string :client_name, :null => false
      t.string :client_company_no
      t.string :client_address_1
      t.string :client_address_2
      t.string :client_town
      t.string :client_postcode
      t.string :client_county
      t.string :client_vat_code

      t.string :self_name
      t.string :self_address_1
      t.string :self_address_2
      t.string :self_town
      t.string :self_postcode
      t.string :self_county

      t.string :self_bank
      t.string :self_account
      t.string :self_sort_code

      t.timestamps
    end
    add_column :invoice_rows, :invoice_id, :integer, :null => false
    add_foreign_key :invoice_rows, :invoices
  end
end
