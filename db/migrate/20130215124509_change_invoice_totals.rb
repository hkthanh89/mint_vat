class ChangeInvoiceTotals < ActiveRecord::Migration
  def change
    change_column :invoices, :vat_rate, :decimal, :precision => 10, :scale => 2
    change_column :invoices, :inv_total, :decimal, :precision => 10, :scale => 2
    change_column :invoices, :inv_vat_total, :decimal, :precision => 10, :scale => 2
    change_column :invoices, :inv_grand_total, :decimal, :precision => 10, :scale => 2
  end
end
