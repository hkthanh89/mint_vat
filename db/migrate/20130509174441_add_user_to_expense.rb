class AddUserToExpense < ActiveRecord::Migration
  def change
  	Expense.delete_all
    add_column :expenses, :user_id, :integer, :null => false
    add_foreign_key :expenses, :users
  end
end
