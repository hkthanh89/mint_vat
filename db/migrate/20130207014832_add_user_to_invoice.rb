class AddUserToInvoice < ActiveRecord::Migration
  def change
    Invoice.delete_all
    add_column :invoices, :user_id, :integer, :null => false
    add_foreign_key :invoices, :users
  end
end
