class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :vat_code
      t.string :address_1
      t.string :address_2
      t.string :town
      t.string :postcode
      t.string :county

      t.string :bank
      t.string :account
      t.string :sort_code

      t.timestamps
    end
    add_column :companies, :user_id, :integer, :null => false
    add_foreign_key :companies, :users
  end
end
