class CreateClients < ActiveRecord::Migration
  def up
    create_table :clients do |t|
      t.string :name
      t.string :vat_code
      t.string :address_1
      t.string :address_2
      t.string :town
      t.string :postcode
      t.string :county

      t.timestamps
    end
    add_column :clients, :user_id, :integer, :null => false
    add_foreign_key :clients, :users
  end

  def down
    drop_table :clients
  end
end
