class AddInvoiceColumns < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.decimal :inv_total, :null => false
      t.decimal :inv_vat_total, :null => false
      t.decimal :inv_grand_total, :null => false
    end
  end
end
