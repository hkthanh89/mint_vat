class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :reference, :null => false
      t.date :date, :null => false
      t.string :description, :null => false
      t.decimal :amount, :null => false, :precision => 10, :scale => 2
      t.decimal :vat, :null => false, :default => 0, :precision => 10, :scale => 2
      t.string :provider
      t.string :provider_vat_regno
      t.text :justification

      t.timestamps
    end
  end
end
