class PaymentDetailsOnInvoice < ActiveRecord::Migration
  def up
    add_column :invoices, :print_payment_details, :boolean, :default => false, :null => false
  end

  def down
    remove_column :invoices, :print_payment_details
  end
end
