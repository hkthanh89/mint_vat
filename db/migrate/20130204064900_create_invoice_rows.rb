class CreateInvoiceRows < ActiveRecord::Migration
  def change
    create_table :invoice_rows do |t|
      t.string :name, :null => false
      t.string :details
      t.string :unit, :limit => 32
      t.decimal :quantity, :null => false, :precision => 10, :scale => 2
      t.decimal :unit_price, :null => false, :precision => 10, :scale => 2
      t.decimal :net_price, :null => false, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
