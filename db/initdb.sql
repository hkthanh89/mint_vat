/*
  Example of initting a mysql database (run this manually)
*/

-- DEV
CREATE USER 'mint_dev'@'localhost' IDENTIFIED BY 'passpass';
create database mint_dev DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
grant all on mint_dev.* to 'mint_dev'@'localhost';
flush privileges;

-- TEST
CREATE USER 'mint_test'@'localhost' IDENTIFIED BY 'passpass';
create database mint_test DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
grant all on mint_test.* to 'mint_test'@'localhost';
flush privileges;

-- STAGE
CREATE USER 'mint_stage'@'localhost' IDENTIFIED BY 'parola1A';
create database mint_stage DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
grant all on mint_stage.* to 'mint_stage'@'localhost';
flush privileges;

-- PROD
CREATE USER 'mint'@'localhost' IDENTIFIED BY 'parola1A';
create database mint DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
grant all on mint.* to 'mint'@'localhost';
flush privileges;
