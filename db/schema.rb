# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130528024348) do

  create_table "clients", :force => true do |t|
    t.string   "name"
    t.string   "vat_code"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "town"
    t.string   "postcode"
    t.string   "county"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id",    :null => false
  end

  add_index "clients", ["user_id"], :name => "clients_user_id_fk"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "vat_code"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "town"
    t.string   "postcode"
    t.string   "county"
    t.string   "bank"
    t.string   "account"
    t.string   "sort_code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id",    :null => false
  end

  add_index "companies", ["user_id"], :name => "companies_user_id_fk"

  create_table "company_events", :force => true do |t|
    t.integer  "company_id",                                                    :null => false
    t.integer  "event_id",                                                      :null => false
    t.date     "date",                                                          :null => false
    t.decimal  "new_flat_rate", :precision => 10, :scale => 2, :default => 0.0, :null => false
    t.decimal  "flat_rate_1",   :precision => 10, :scale => 2, :default => 0.0, :null => false
    t.decimal  "flat_rate_2",   :precision => 10, :scale => 2, :default => 0.0, :null => false
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
  end

  add_index "company_events", ["company_id"], :name => "company_events_company_id_fk"
  add_index "company_events", ["event_id"], :name => "company_events_event_id_fk"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "expenses", :force => true do |t|
    t.string   "reference",                                                          :null => false
    t.date     "date",                                                               :null => false
    t.string   "description",                                                        :null => false
    t.decimal  "amount",             :precision => 10, :scale => 2,                  :null => false
    t.decimal  "vat",                :precision => 10, :scale => 2, :default => 0.0, :null => false
    t.string   "provider"
    t.string   "provider_vat_regno"
    t.text     "justification"
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.integer  "user_id",                                                            :null => false
  end

  add_index "expenses", ["user_id"], :name => "expenses_user_id_fk"

  create_table "invoice_rows", :force => true do |t|
    t.string   "name",                                                    :null => false
    t.string   "details"
    t.string   "unit",       :limit => 32
    t.decimal  "quantity",                 :precision => 10, :scale => 2, :null => false
    t.decimal  "unit_price",               :precision => 10, :scale => 2, :null => false
    t.decimal  "net_price",                :precision => 10, :scale => 2, :null => false
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.integer  "invoice_id",                                              :null => false
  end

  add_index "invoice_rows", ["invoice_id"], :name => "invoice_rows_invoice_id_fk"

  create_table "invoices", :force => true do |t|
    t.string   "number",                                                                  :null => false
    t.date     "date",                                                                    :null => false
    t.decimal  "vat_rate",              :precision => 10, :scale => 2, :default => 0.0,   :null => false
    t.date     "due_date"
    t.string   "client_name",                                                             :null => false
    t.string   "client_company_no"
    t.string   "client_address_1"
    t.string   "client_address_2"
    t.string   "client_town"
    t.string   "client_postcode"
    t.string   "client_county"
    t.string   "client_vat_code"
    t.string   "self_name"
    t.string   "self_address_1"
    t.string   "self_address_2"
    t.string   "self_town"
    t.string   "self_postcode"
    t.string   "self_county"
    t.string   "self_bank"
    t.string   "self_account"
    t.string   "self_sort_code"
    t.datetime "created_at",                                                              :null => false
    t.datetime "updated_at",                                                              :null => false
    t.decimal  "inv_total",             :precision => 10, :scale => 2,                    :null => false
    t.decimal  "inv_vat_total",         :precision => 10, :scale => 2,                    :null => false
    t.decimal  "inv_grand_total",       :precision => 10, :scale => 2,                    :null => false
    t.integer  "user_id",                                                                 :null => false
    t.boolean  "print_payment_details",                                :default => false, :null => false
  end

  add_index "invoices", ["user_id"], :name => "invoices_user_id_fk"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "client_ip"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_foreign_key "clients", "users", :name => "clients_user_id_fk"

  add_foreign_key "companies", "users", :name => "companies_user_id_fk"

  add_foreign_key "company_events", "companies", :name => "company_events_company_id_fk"
  add_foreign_key "company_events", "events", :name => "company_events_event_id_fk"

  add_foreign_key "expenses", "users", :name => "expenses_user_id_fk"

  add_foreign_key "invoice_rows", "invoices", :name => "invoice_rows_invoice_id_fk"

  add_foreign_key "invoices", "users", :name => "invoices_user_id_fk"

end
