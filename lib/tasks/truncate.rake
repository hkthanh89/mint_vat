namespace :truncate do
  desc "Truncate table events"
  task :events => :environment do
  	ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS=0")
  	ActiveRecord::Base.connection.execute("TRUNCATE TABLE events")
  	ActiveRecord::Base.connection.execute("SET FOREIGN_KEY_CHECKS=1")
  end
end
